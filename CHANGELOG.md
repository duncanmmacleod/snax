# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

### Fixed

- Fix issue where more streams can be assigned to an extract job than
  max_streams allows in configuration

## [0.5.1] - 2022-02-21

### Added

- Add packaging for Rocky Linux 8

## [0.5.0] - 2022-02-17

- Migrate SNAX from gstlal-burst

### Added

- Add devshm in list of available live data sources
- Allow use of guardian state to filter what data is processed

### Changed

- Change `--request-timeout` to `--query-timeout` to avoid Condor variable clashing
- Change to Makefile template for offline analysis, following GstLAL analyses
- Change default name for filename from GSTLAL_SNAX to SNAX
- Use Buffer class to allow comparisons between features to determine priority

### Removed

- `snax_aggregate`: Remove hdf5 backend for scald

### Fixed

- Fix timestamps in messages sent via Kafka to use integer milliseconds
- Fix issue in online workflow where Kafka was not used to transfer features if
  requested
- `snax_bank_overlap`: fix variable naming issues in plotting waveforms
- `snax_combine`: fix case when `--outdir` is not set
- `snax_sink`: fix issue with loading channels from configuration file
- Define channel names with frequency range information consistently
- Pass in sample rate to synchronize job
- Check default case for including sections for channels in datasource
- `snax_bank_overlap`: Fix matplotlib import for 3.6+
- `snax_synchronize`: Fix off-by-one error in listening to topics
- `snax_sink`: Skip writing zero-size files to disk
- `snax_sink`: Fix duration length in filenames in an edge case

[unreleased]: https://git.ligo.org/snax/snax/-/compare/v0.5.1...main
[0.5.1]: https://git.ligo.org/snax/snax/-/tags/v0.5.1
[0.5.0]: https://git.ligo.org/snax/snax/-/tags/v0.5.0
