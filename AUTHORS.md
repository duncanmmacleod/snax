## Maintainers

| Name                | Email                                 |
|---------------------|---------------------------------------|
| Patrick Godwin      | <patrick.godwin@ligo.org>             |

## Contributors

| Name                | Email                                 |
|---------------------|---------------------------------------|
| Kipp Cannon         | <kipp.cannon@ligo.org>                |
| Sydney Chamberlin   | <syd.chamberlin@gmail.com>            |
| Chad Hanna          | <chad.hanna@ligo.org>                 |
| Drew Keppel         | N/A                                   |
| Duncan Meacher      | <duncan.meacher@ligo.org>             |
