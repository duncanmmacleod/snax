============
Installation
============

.. contents::
   :local:

To install with pip:

.. code:: bash

   pip install .
