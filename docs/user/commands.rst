==========
Commands
==========

.. contents::
   :local:


**snax** provides a command line interface for various tasks:

* :code:`snax_extract`: extract features from auxiliary channel data

:code:`snax_extract`
~~~~~~~~~~~~~~~~~~~~~~

This program allows a user to extract features from auxiliary channel data.

.. program-output:: snax_extract --help
   :nostderr:
