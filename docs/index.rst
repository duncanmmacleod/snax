SNAX documentation
===========================

:code:`snax` is a Stream-based Noise Acquisition and eXtraction toolkit

User's Guide
------------

.. toctree::
   :maxdepth: 1

   user/installation
   user/quickstart
   user/commands

API Reference
-------------

.. toctree::
   :maxdepth: 2

   api/api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
